/*
    Copyright (C) 2023 Thomas Civade (UGA)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
*/

/**
 * Index file to group all exports of audeClient module
 * @author Thomas Civade
 * @module audeClient 
 */

const {
    Automaton, 
    Transition
} = require("./js/automaton");

const { 
    concatenate, 
    BFS,
    complete,
    complement,
    isCompleted,
    coreachableStates,
    isCoreachable,
    infiniteLanguage,
    emptyLanguage,
    unionAutomaton,
    DFS, 
    determinize,
    isDeterminized, 
    difference, 
    epsElimination, 
    minimize, 
    isMinimized, 
    normalize, 
    automaton2regex, 
    product,
    automaton2unixregex, 
    regex2automaton, 
    unixregex2automaton, 
    reachableStates, 
    isReachable, 
    equivalence, 
    include, 
    mirror,
    store, 
    search, 
    load, 
    remove,
    register,
    getToken
} = require("./js/client");

const {
    automaton2json,
    json2automaton
} = require("./js/automaton2json");

module.exports = {
    Automaton, 
    Transition,
    concatenate, 
    BFS,
    complete,
    complement,
    isCompleted,
    coreachableStates,
    isCoreachable,
    infiniteLanguage,
    emptyLanguage,
    unionAutomaton,
    DFS, 
    determinize,
    isDeterminized, 
    difference, 
    epsElimination, 
    minimize, 
    isMinimized, 
    normalize, 
    automaton2regex, 
    product,
    automaton2unixregex, 
    regex2automaton, 
    unixregex2automaton, 
    reachableStates, 
    isReachable, 
    equivalence, 
    include, 
    mirror,
    store, 
    search, 
    load, 
    remove,
    automaton2json,
    json2automaton,
    register,
    getToken
};