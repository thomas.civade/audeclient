AUTOMATON & TRANSITION DOCUMENTATION
============

- [Create an automaton](#createautomaton)

- [Manipulate states](#manipulatestates)
- [Manipulate transitions](#manipulatetrans)
- [Manipulate alphabet](#manipulatealphabet)
- [Execute a word](#testaword)
- [Copy the automaton](#deepcopyautomaton)

- [Create a transition](#createtransition)
# Automaton

A class to manipulate automata in JavaScript.

## Constructor <a id="createautomaton"></a>

```javascript
new Automaton(states?, Sigma?, qInit?, trans?, finalStates?);
```
----
## Properties

### states

- Type: Set
- Description: The set of states in the automaton.

### Sigma

- Type: Set
- Description: The set of symbols in the alphabet of the automaton.

### qInit

- Type: any
- Description: The initial state of the automaton.

### trans

- Type: Set
- Description: The set of transitions in the automaton.

### finalStates

- Type: Set
- Description: The set of final (accepting) states in the automaton.

### currentStates

- Type: Set
- Description: The set of current states (used for some operations).

### lastTakenTransitions

- Type: Set
- Description: The set of last taken transitions (used for some operations).
---
## Methods

### addState <a id="manipulatestates"></a>
- **Parameters**
    - **state** : any, the statename
    - **final** : Boolean, 
        - if True : the state added is set has final state
        - Otherwise, the state isn't a final one. 
```javascript
addState(state, final);
```

This method adds a state to the automaton.

### addFinalState
- **Parameters**
    - **state** : any, the statename
```javascript
addFinalState(state);
```

This method adds a final (accepting) state to the automaton. If the state is already there, makes it final.

### addAcceptingState

```javascript
addAcceptingState(state);
```

This method is an alias of the `addFinalState` method.

### setFinalState

```javascript
setFinalState(state);
```

This method is an alias of the `addFinalState` method.

### setNonFinalState
- **Parameters**
    - **state** : any, the statename
```javascript
setNonFinalState(state);
```

This method removes a state from the set of final states of the automaton.

### toggleFinalState
- **Parameters**
    - **state** : any, the statename
```javascript
toggleFinalState(state);
```

This method toggles a state in the set of final states of the automaton.

### setAcceptingState

```javascript
setAcceptingState(state);
```

This method is an alias of the `addFinalState` method.

### toggleAcceptingState

```javascript
toggleAcceptingState(state);
```

This method is an alias of the `toggleFinalState` method.

### setNonAcceptingState

```javascript
setNonAcceptingState(state);
```

This method is an alias of the `setNonFinalState` method.

### getNonFinalStates
- **Return**
    - **Set** : set of non final states
```javascript
getNonFinalStates();
```

This method returns the set of non-final (accepting) states of the automaton.

### getNonAcceptingStates

```javascript
getNonAcceptingStates();
```

This method is an alias of the `getNonFinalStates` method.

### getStates
- **Return**
    - **Set** : set of states
```javascript
getStates();
```

This method returns the set of states of the automaton.

### setStates
- **Parameters**
    - **states** : Set/Array, a set/array of states
    - **dontCopy** : Boolean, If you don t want the function to copy the given Set of state, Set this to true; the Set will be used directly.
```javascript
setStates(states, dontCopy);
```

This method sets the set of states of the automaton.

### getFinalStates
- **Return**
    - **Set** : set of final states
```javascript
getFinalStates();
```

This method returns the set of final (accepting) states of the automaton.

### getAcceptingStates

```javascript
getAcceptingStates();
```

This method is an alias of the `getFinalStates` method.

### setFinalStates
- **Parameters**
    - **states** : Set/Array, a set/array of states
    - **dontCopy** : Boolean, If you don t want the function to copy the given Set of state, Set this to true; the Set will be used directly.
```javascript
setFinalStates(states, dontCopy);
```

This method sets the set of final (accepting) states of the automaton. Every other state is set to non-final.

### setAcceptingStates

```javascript
setAcceptingStates(states);
```

This method is an alias of the `setFinalState` method.

### setInitialState
- **Parameters**
    - **state** : any, statename
```javascript
setInitialState(state);
```

This method sets the initial state of the automaton.

### getInitialState
- **Return**
    - **any** : initial statename
```javascript
getInitialState();
```

This method returns the initial state of the automaton.

### removeState
- **Parameters**
    - **states** : any, statename
```javascript
removeState(state);
```

This method removes a state from the automaton.

### hasState
- **Parameters**
    - **state** : any, a statename
- **Return**
    - **Boolean** 
        - true if the state is in the automaton
        - false otherwise
```javascript
hasState(state);
```

This method checks if the automaton has the state given in the parameter.

### isFinalState
- **Parameters**
    - **state** : any, a statename
- **Return**
    - **Boolean** 
        - true if the state is a final state
        - false otherwise
```javascript
isFinalState(state);
```

This method checks if the state given in the parameter is a final state of this automaton.

### isAcceptingState

```javascript
isAcceptingState(state);
```

This method is an alias of the `isFinalState` method.

----
### addTransition <a id="manipulatetrans"></a>
- **Parameters**
    - **t** : any, the start state name
    - **t1** : any, a symbol
    - **t2** : any, the end state name
```javascript
addTransition(t, t1, t2);
```

This method adds a transition to the automaton.

### removeTransition
- **Parameters**
    - **t** : any, the start state name
    - **t1** : any, a symbol
    - **t2** : any, the end state name
```javascript
removeTransition(t, t1, t2);
```

This method removes a transition from the automaton.

### hasTransition
- **Parameters**
    - **t** : any, the start state name
    - **t1** : any, a symbol
    - **t2** : any, the end state name
- **Return**
    - **Boolean**
        - true if the transition exist in the automaton
        - false otherwise
```javascript
hasTransition(t, t1, t2);
```

This method checks if a transition exists in the automaton.

### getTransitions
- **Return**
    - **Set** : set of transitions
```javascript
getTransitions();
```

This method returns the set of transitions of the automaton.

### setTransitions
- **Parameters**
    - **trans** : Set, set of transitions
```javascript
setTransitions(trans);
```

This method sets the set of transitions of the automaton.

### hasEpsilonTransitions
- **Return**
    - **Boolean** : 
        - true if the automaton has an epsilon transition
        - false otherwise

```javascript
hasEpsilonTransitions();
```

This method test if the automaton has an epsilon transition.

### getTransitionFunction
- **Parameters**
    - **determinizedFunction** : Boolean
        - if true: return the determinized version of the transition function (can only have 1 state as result)
        - else: return the non determinized version of the transition function (can return multiple states in one transition)

- **Return**
    - **Function** the transition function
```javascript
getTransitionFunction(determinizedFunction);
```

This method returns the transition function of the automaton. This function is such that:
- f() returns the set of start states
- f(startState) return the set of symbols such that one more (startState, symbol, endState) transitions exist(s)
- f(startState, symbol) returns the set of states reachable with (startState, symbol). If determinizedFunction is true, return the only state reachable with (startState, symbol).
- f(null, null, true) returns the set of endStates of all transitions.


### getAlphabet <a id="manipulatealphabet"></a>
- **Return**
    - **Set** : set of symbols
```javascript
getAlphabet();
```

This method returns the Set of symbols of the Automaton
### setAlphabet
- **Parameters**
    - **alphabet** : Set, the set of symbol to use
    - **byRef** : Boolean
        - if true, do not recreate a copy of the alphabet set
        - otherwise create a copy. 
```javascript
setAlphabet(alphabet, byRef);
```
This method sets the Set of symbols of the automaton.



### addAlphabet
- **Parameters**
    - **alphabet** : Set, a set of symbol

```javascript
addAlphabet(alphabet);
```

This method set the automaton alphabet as the union of the actual alphabet and the parameter alphabet set. 


### removeAlphabet
- **Parameters**
    - **alphabet** : Set, a set of symbol

```javascript
removeAlphabet(alphabet);
```

This method set the automaton alphabet as  the actual alphabet minus the parameter alphabet set. 

### addSymbol
- **Parameters**
    - **symbol** : Any, a symbol
```javascript
addSymbol(symbol);
```
This method add a symbol to the automaton alphabet.

### hasSymbol
- **Parameters**
    - **symbol** : Any, a symbol
- **Return**
    - **Boolean**
        - true if the automaton has the symbol in its alphabet
        - false otherwise
```javascript
hasSymbol(symbol);
```
This method tests whether a symbol belongs to the automaton.

### removeSymbol
- **Parameters**
    - **symbol** : Any, a symbol
```javascript
removeSymbol(symbol);
```
This method remove a symbol from the automaton alphabet.

### toString
```javascript
toString();
```
This method returns a string representation of the automaton.

### setCurrentState
- **Parameters**
    - **state** : any, a statename
```javascript
setCurrentState(state);
```
This method sets the current state of the automaton

### addCurrentState
- **Parameters**
    - **state** : any, a statename
```javascript
addCurrentState(state);
```
This method add a state to the current states set

### removeCurrentState
- **Parameters**
    - **state** : any, a statename
```javascript
removeCurrentState(state);
```
This method remove a state from the current states set

### addCurrentStates
- **Parameters**
    - **states** : Set, a set of states
```javascript
addCurrentStates(states);
```
This method add states to the current states set

### removeCurrentStates
- **Parameters**
    - **states** : Set, a set of state
```javascript
removeCurrentStates(states);
```
This method remove states tofrom the current states set


### getCurrentStates
- **Return**
    - **Set** : a set of state
```javascript
getCurrentStates();
```
This method return the set of current states

### currentStatesAddReachablesByEpsilon
- **Parameters**
    - **transitionFunction** : Function, the transition function
    - **visited** : Set of states already visited
```javascript
currentStatesAddReachablesByEpsilon(transitionFunction, visited);
```
This methods looks at current states and transitions of the Automaton to add all states reachable with epsilon to the current states.

### getSuccessors
- **Parameters**
    - **state** : any, a state name
    - **symbol** : any, a symbol
- **Return**
    - **Set** : a set of end states
```javascript
getSuccessors(state, symbol);
```
This methods returns the set of successors of a state. Its behavior is well defined only on determinized automata.

### getReachable
- **Parameters**
    - **state** : any, a state name
    - **visited** : Set, a set of exclude states
- **Return**
    - **Set** : a set of reachable states
```javascript
getReachable(state, visited);
```
This methods return a set of reachable states

### runSymbol
- **Parameters**
    - **symbol** : any, a symbol
    - **transitionFunction** : Function, the function produce by `GetTransitionFunction`
    - **dontEraseTakenTransitions** : Boolean
        - if true, already taken transitions aren't erase
        - else they are. 
```javascript
runSymbol(symbol, transitionFunction, dontEraseTakenTransitions);
```
This methods looks at current states and transitions of the Automaton to replace current states by all states reachable with the given symbol.

### runWord
- **Parameters**
    - **symbols** : any, symbols
```javascript
runWord(symbols);
```
This method runs each symbol of the list of symbol given. See the description of the runSymbol for more information. Don't forget to Set the current state to the initial state if you need it.

### acceptedWord <a id="testaword"></a>
- **Parameters**
    - **symbols** : any, symbols
- **Return**
    - **Boolean** :
        - true if the execution finish on at least one final state
        - false otherwise
```javascript
acceptedWords(symbols);
```
This method runs a word from the initial state and checks if this word is accepted by the automaton. It takes care to restore current states and last taken transitions after the run.

### getLastTakenTransitions
- **Return**
    - **Set** : set of last taken transitions
```javascript
getLastTakenTransitions();
```
This method returns the Set of transitions that were taken while running on the last symbol. See the runSymbol method for more information.

### copy <a id="deepcopyautomaton"></a>
- **Return**
    - **Automaton** : a copy of the actual automaton
```javascript
copy();
```

This method return a copy of the actual automaton. 

---


# Transition 
A class to manipulate transitions of automata in Javascript.

---
## Constructor <a id ="createtransition"></a>

```javascript
new Transition(startState, symbol, endState);
```
----

## Properties

### startState

- Type: any
- Description: The start state of the transition

### symbol

- Type: any
- Description: The symbol of the transition

### qInit

- Type: any
- Description: The end state of the transition

---
## Methods

### toString
- **Return**
    - **String** : the string version of the transition
```javascript
toString();
```

This method return the string version of a transition. 