/*
  Copyright (C) 2023 Thomas Civade (UGA)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.
  */

/**
 * @author  Thomas Civade
 * @file This is a library to format automaton in JSON to use them in the aude api.
 */

"use strict";

const {Automaton} = require('./automaton');


/**
 * This method transform a list of Transition object in a fewer form
 * Ex : [[1,'a',2],[1,'b',2],[2,'a',1]] => [[1,['a','b'],2],[2,['a'],1]]
 *
 * @method
 * @param {Array} transitionsList this list of Transition object
 * 
 * @returns {Array} the new array of transition
 */
function groupTransitions(transitionsList) {
    // Create a dictionary to store the grouped symbols and end states
    const groupedTransitions = {};
  
    // Iterate over the list of transitions
    for (const transition of transitionsList) {
        const [startState, symbol, endState] = [transition.startState, transition.symbol, transition.endState];
        const key = [startState, endState];
        // Check if the start state exists in the dictionary
        if (!(key in groupedTransitions)) {
            groupedTransitions[key] = {
                symbols:[],
                startState:startState,
                endState:endState
            }
    }
  
      // Add the symbol to the list of symbols for the start state
      groupedTransitions[key].symbols.push(symbol);
    }
  
    // Convert the dictionary into a list of grouped transitions
    const groupedTransitionsList = [];
    for (const key in groupedTransitions) {
        const [startState, endState] = [groupedTransitions[key].startState, groupedTransitions[key].endState];
        const symbols = groupedTransitions[key].symbols;
        groupedTransitionsList.push([startState, symbols, endState]);
    }
    return groupedTransitionsList;
  }
  
/**
 * This method transform an automaton Object into his JSON form
 *
 * @method
 * 
 * @param {Automaton} a the automaton to transform
 * 
 * @returns {Object} a JSON equivlaent automaton
 */
function automaton2json(a) {
    //console.log(a.getTransitions().getList());
    var initialstate = a.getInitialState();
    var normal_states = a.getNonFinalStates().getList();
    var final_states = a.getFinalStates().getList();
    var transition_list = groupTransitions(a.getTransitions().getList());
    var result = {  
        "initialState":null,
        "normalStates":[],
        "finalStates":[],
        "transitions":[]
    }
    if (initialstate !== null) result["initialState"] = initialstate;
    if (normal_states) result["normalStates"] = normal_states.filter(value => value !== null);
    if (final_states) result["finalStates"] = final_states.filter(value => value !== null);
    if (transition_list) result["transitions"] = transition_list
    return result
}

/**
 * This method transform a JSON into his automaton Object form
 *
 * @method
 * 
 * @param {Object} j the JSON to transform
 * 
 * @returns {Automaton} the automaton resulting of the j Object
 */
function json2automaton(j){
    const a = new Automaton();
    a.setInitialState(j["initialState"]);
    for (var s of j["normalStates"]) a.addState(s);
    for (var s of j["finalStates"]) a.addFinalState(s);
    for (var t of j["transitions"]){
            a.addTransition(t[0],t[1],t[2])
    }
    return a;
}

module.exports = {
    automaton2json,
    json2automaton
};