/*
    Copyright (C) 2023 Thomas Civade (UGA)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
*/

/**
 * A test file to verify if the audeAPI and audeClient work well
 * @author Thomas Civade
 * @file 
 */

'use strict';


const { 
    Automaton,
    concatenate, 
    BFS,
    complete,
    complement,
    isCompleted,
    coreachableStates,
    isCoreachable,
    infiniteLanguage,
    emptyLanguage,
    unionAutomaton,
    DFS, 
    determinize,
    isDeterminized, 
    difference, 
    epsElimination, 
    minimize, 
    isMinimized, 
    normalize, 
    automaton2regex, 
    product,
    automaton2unixregex, 
    regex2automaton, 
    unixregex2automaton, 
    reachableStates, 
    isReachable, 
    equivalence, 
    include, 
    mirror,
    store, 
    search, 
    load, 
    remove
} = require("../AUDEclient");

// Provide your token here !
const token = "geNU0bzohWJcbOj9KNJk"

/**
 * Method to test equivalence between array
 * @method
 * @param {Array} arr1 
 * @param {Array} arr2 
 * @returns 
 */
function areArraysEquivalent(arr1, arr2) {
    if (arr1.length !== arr2.length) {
      return false;
    }
  
    const sortedArr1 = arr1.slice().sort();
    const sortedArr2 = arr2.slice().sort();
  
    return JSON.stringify(sortedArr1) === JSON.stringify(sortedArr2);
  }

async function test_BFS_DFS(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',2);
    A.addTransition(1,'a',3);
    A.addTransition(1,'a',4);
    A.addTransition(3,'a',4);
    A.addTransition(4,'a',5);
    A.addTransition(5,'a',6);
    const BFS_states = await BFS(A,token);
    const DFS_states = await DFS(A,token);
    if (!areArraysEquivalent(BFS_states,[1,2,3,4,5,6])){
        throw new Error("Error in BFS_DFS test 1");
    } 
    if (!areArraysEquivalent(DFS_states,[1,4,5,6,3,2])){
        throw new Error("Error in BFS_DFS test 2");
    } 
}

async function test_concatenate(){
    const A = new Automaton();
    A.setInitialState(1);
    A.setFinalState(2)
    A.addTransition(1,'a',2);
    const B = new Automaton();
    B.setInitialState(3);
    B.setFinalState(4);
    B.addTransition(3,'b',4);
    const AB = await concatenate(A,B,token);
    if (!AB.acceptedWord("ab")){
        throw new Error("Error in concatenate test 1");
    } 
    if (AB.acceptedWord("ba")){
        throw new Error("Error in concatenate test 2");
    } 
    if (AB.acceptedWord("abb")){
        throw new Error("Error in concatenate test 3");
    } 

}

async function test_complete(){
    const A = new Automaton()
    A.setInitialState(1);
    A.setFinalState(2); 
    A.addTransition(1,'a',2);
    const B = await complete(A,token);
    const A_isComplete = await isCompleted(A,token);
    const B_isComplete = await isCompleted(B,token);
    if (A_isComplete){
        throw new Error("Error in complete test 1");
    } 
    if (!B_isComplete){
        throw new Error("Error in complete test 2");
    } 
}

async function test_complement(){
    const A = new Automaton();
    A.setInitialState(1);
    A.setFinalState(2); 
    A.addTransition(1,'a',2);    
    const B = await complement(A,token);
    if (!A.acceptedWord("a")){
        throw new Error("Error in complement test 1");
    } 
    if (!!B.acceptedWord("a")){
        throw new Error("Error in complement test 2");
    } 
}

async function test_coreachability(){
    const A = new Automaton();
    A.setInitialState(1)
    A.addTransition(1,'a',2);
    A.addTransition(2,'a',3);
    A.addTransition(4,'a',2);
    A.setFinalState(3);
    const states = await coreachableStates(A,token);
    if (!areArraysEquivalent(states, [ 3, 2, 1, 4 ])){
        throw new Error("Error in coreachability test 1");
    } ;
    if (!await isCoreachable(A,token)){
        throw new Error("Error in coreachability test 2");
    } ;
    A.addState('S');
    if (!!await isCoreachable(A,token)){
        throw new Error("Error in coreachability test 3");
    } ;
}

async function test_determinize(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',2);
    A.addTransition(1,'a',1);
    const B = await determinize(A,token);
    if (await isDeterminized(A,token)){
        throw new Error("Error in determinize test 1");
    } ;
    if (!await isDeterminized(B,token)){
        throw new Error("Error in determinize test 2");
    } ;
}

async function test_difference(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,['a','b'],2);
    A.setAcceptingState(2);
    const B = new Automaton();
    B.setInitialState(1);
    B.addTransition(1,['b','c'],2);
    B.setAcceptingState(2);
    const diffAB = await difference(A,B,token);
    if (!diffAB.acceptedWord("a")){
        throw new Error("Error in difference test 1");
    } ;
    if (diffAB.acceptedWord("b")){
        throw new Error("Error in difference test 2");
    } ;
}

async function test_language(){
    const A = new Automaton();
    A.setInitialState(1);
    A.setAcceptingState(2);
    A.addTransition(1,'a',1);
    if (!await emptyLanguage(A,token)){
        throw new Error("Error in language test 1");
    } ;
    if (await infiniteLanguage(A,token)){
        throw new Error("Error in language test 2");
    } ;
    A.addTransition(1,'a',2);
    if (await emptyLanguage(A,token)){
        throw new Error("Error in language test 3");
    } ;
    if (!await infiniteLanguage(A,token)){
        throw new Error("Error in language test 4");
    } ;
}

async function test_epselim(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',1);
    A.addTransition(1,'\\e',2);
    A.setFinalState(2);
    const B = await epsElimination(A,token);
    if (!B.isFinalState(0)){
        throw new Error("Error in epselim test 1");
    } ;
}

async function test_minimize(){
    const A = new Automaton();
    A.setInitialState(1)
    A.addTransition(1,'a',2);
    A.addTransition(2,'a',2);
    A.addTransition(2,'a',3);
    A.addTransition(3,'\\e',1);
    A.addFinalState(1);
    A.addFinalState(2);
    A.addFinalState(3);
    const B = await minimize(A,token);
    if (await isMinimized(A,token)){
        throw new Error("Error in minimize test 1");
    } ;
    if (!await isMinimized(B,token)){
        throw new Error("Error in minimize test 2");
    } ;
}

async function test_normalize(){
    const A = new Automaton();
    A.setInitialState([1,2],'a',[1,2]);
    A.setInitialState([2],'a',[1]);
    A.setInitialState([1,2,3],'a',[1,2]);
    A.setInitialState([1],'a',[1,2]);
    const B = await normalize(A,token);
    if (areArraysEquivalent(A.getStates().getList(),[0,1,2,3])){
        throw new Error("Error in normalize test 1");
    } ;
    if (!areArraysEquivalent(B.getStates().getList(),[0,1,2,3])){
        throw new Error("Error in normalize test 2");
    } ;
}

async function test_regex(){
    const A =  await regex2automaton("a + b + \\e",token)
    const B =  await unixregex2automaton("^a*a$",token)
    const re = await automaton2regex(A,token);
    const unixre = await automaton2unixregex(B,token);
    if (re !== "ε + a + b"){
        throw new Error("Error in test_regex test 1");
    } ;
    if (unixre !== "^a+$"){
        throw new Error("Error in test_regex test 2");
    } ;
}

async function test_reachability(){
    const A = new Automaton();
    A.setInitialState(1)
    A.addTransition(1,'a',2);
    A.addTransition(1,'a',3);
    A.addTransition(2,'b',4);
    A.addTransition(3,'a',3);
    A.addTransition(5,'a',1);
    const states = await reachableStates(A,token);
    if (!areArraysEquivalent(states,[1,2,3,4])){
        throw new Error("Error in reachability test 1");
    } ;
    if (await isReachable(A,token)){
        throw new Error("Error in reachability test 2");
    } ;
    A.addTransition(1,'a',5);
    if (!await isReachable(A,token)){
        throw new Error("Error in reachability test 3");
    } ;
}

async function test_equivalence(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',2);
    A.addTransition(1,'b',3);
    A.addTransition(1,'c',4);
    A.addTransition(2,'',5);
    A.addTransition(3,'',5);
    A.addTransition(4,'',5);
    A.addTransition(5,['a','b','c'],6);
    A.setFinalState(6)
    const B = new Automaton();
    B.setInitialState(1);
    B.addTransition(1,['a','b','c'],2);
    B.addTransition(2,['a','b','c'],3);
    B.setFinalState(3);
    if (!await equivalence(A,B,token)){
        throw new Error("Error in equivalence test 1");
    } ;
    A.addFinalState(5);
    if (await equivalence(A,B,token)){
        throw new Error("Error in equivalence test 2");
    } ;
}

async function test_inclusion(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',2);
    A.addAcceptingState(2);
    const B = new Automaton();
    B.setInitialState(1);
    B.addTransition(1,['a','b'],1);
    B.addAcceptingState(1);
    if (!await include(A,B,token)){
        throw new Error("Error in inclusion test 1");
    } ;
    if (await include(B,A,token)){
        throw new Error("Error in inclusion test 2");
    } ;

}

async function test_mirror(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,'a',2);
    A.addTransition(2,'b',3);
    A.addTransition(3,'c',1);
    A.addFinalState(3);
    const B = await mirror(A,token);
    if (B.acceptedWord("ab")){
        throw new Error("Error in mirror test 1");
    } ;
    if (!B.acceptedWord("ba")){
        throw new Error("Error in mirror test 2");
    } ;
    if (!B.acceptedWord("bacba")){
        throw new Error("Error in mirror test 3");
    } ;
}

async function test_product_union(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,['a','b'],2);
    A.setFinalState(2)
    const B = new Automaton();
    B.setInitialState(1);
    B.addTransition(1,['b','c'],2);
    B.setFinalState(2)
    const C = await product(A,B,token);
    const D = await unionAutomaton(A,B,token);
    if (C.acceptedWord("a")){
        throw new Error("Error in product_union test 1");
    } ;
    if (C.acceptedWord("c")){
        throw new Error("Error in product_union test 2");
    } ;
    if (!C.acceptedWord("b")){
        throw new Error("Error in product_union test 3");
    } ;
    if (!D.acceptedWord("a")){
        throw new Error("Error in product_union test 4");
    } ;
    if (!D.acceptedWord("b")){
        throw new Error("Error in product_union test 5");
    } ;
    if (!D.acceptedWord("c")){
        throw new Error("Error in product_union test 6");
    } ;
}

async function test_memory(){
    const A = new Automaton();
    A.setInitialState(1);
    A.addTransition(1,['a','b'],2);
    A.setFinalState(2)
    if (!await store(A,"my_new_automaton",token)){
        throw new Error("Error in memory test 1");
    } ;
    const titles = await search(token,null,true,true);
    if (titles.length === 0){
        throw new Error("Error in memory test 2");
    } ;
    const B = await load("my_new_automaton",token);
    if (!await remove(titles[0],token)) {
        throw new Error("Error in memory test 3");
    } ;
}


test_BFS_DFS();
test_concatenate();
test_complete();
test_complement()
test_coreachability();
test_determinize();
test_difference();
test_language();
test_epselim();
test_minimize();
test_normalize();
test_regex();
test_reachability();
test_equivalence();
test_inclusion();
test_mirror();
test_product_union();
test_memory();
