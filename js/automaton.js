/*
    Copyright (c) 2013-2014, Raphaël Jakse (Université Joseph Fourier)
    All rights reserved.
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Université Joseph Fourier nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @author  Thomas Civade
 * @module automaton this module define the Automaton class
 */
'use strict';

const {toArray, toSet} = require('./aude');
const {elementToString, Set} = require('./set');


/**
 * epsilon: Represents epsilon to manipulate epsilon transitions.
 * epsilon is a function to enforce equality to be true when and only when comparing explicitly with epsilon.
 * @alias epsilon
 */
function is_epsilon(i) { return ["\\e","ε",""].includes(i); };
function epsilon_toString() { return "ε"; };
const epsilon = "ε";


/**
 * A class to manipulate automata in Javascript.

* @class
* @alias Automaton
*/
class Automaton {
    constructor(states, Sigma, qInit, trans, finalStates) {
        if (states) {
            if (states instanceof Automaton) {
                Automaton.call(this, new Set(states.states), new Set(states.Sigma), states.qInit, new Set(states.trans), new Set(states.finalStates));
                this.currentStates = new Set(states.currentStates);
                this.lastTakenTransitions = new Set(states.lastTakenTransitions);
                return;
            }

            if (!(states instanceof Array) && !(states instanceof Set)) {
                throw new Error(("Automaton constructor takes an Automaton in argument, or nothing."));
            }
        }

        this.states = toSet(states);
        this.Sigma = toSet(Sigma);
        this.qInit = qInit;

        this.setTransitions(trans);

        this.finalStates = toSet(finalStates);

        if (!this.currentStates) {
            this.currentStates = new Set();
            this.lastTakenTransitions = new Set();
        }
    }

    format = function (s, i) { return s.replace("{0}", i); };

   
    /**
     * This method adds a state to the automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The state to add, can be of any type.
     * @param {boolean} [final] If given and true, the state will be a final (accepting) state.
     * @see addFinalState
     * @see addAcceptingState
     */

    addState(state, final) {
        this.states.add(state);
        if (final) {
            this.finalStates.add(state);
        }
    }

    /**
     * This method adds a final (accepting) state to the automaton. If the state is already there, makes it final.
     * @method
     * @memberof Automaton
     * @param {any} state The state to add, can be of any type.
     * @see Automaton#setNonFinalState
     */

    addFinalState(state) {
        this.addState(state, true);
    }

    /**
     * This method is an alias of the addFinalStateState method.
     * @see Automaton#addFinalState
     */
    addAcceptingState(state) {
        this.addState(state, true);
    }

    /**
     * This method is an alias of the addFinalState method.
     * @see Automaton#setNonFinalState
     */
    setFinalState(state) {
        this.addState(state, true);
    }

    /**
     * This method removes a state from the Set of final states of the automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The state to add, can be of any type.
     * @see Automaton#addFinalState
     */
    setNonFinalState(state) {
        this.finalStates.remove(state);
    }

    /**
     * This methodes toggles a state in the Set of final states of the automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The state to add or remove, can be of any type.
     */
    toggleFinalState(state) {
        if (this.finalStates.has(state)) {
            this.setNonFinalState(state);
        } else {
            this.setFinalState(state);
        }
    }

    /**
     * This method is an alias of the addFinalStateState method.
     * @method
     * @memberof Automaton
     * @see Automaton#addFinalState
     */
    setAcceptingState(state) {
        this.addState(state, true);
    }

    /**
     * This method is an alias of the toggleFinalState method.
     * @method
     * @memberof Automaton
     * @see Automaton#toggleFinalState
     */
    toggleAcceptingState(state) {
        this.toggleFinalState(state);
    }

    /**
     * This method is an alias of the setNonFinalState method.
     * @method
     * @memberof Automaton
     * @see Automaton#setNonFinalState
     */
    setNonAcceptingState(state) {
        this.finalStates.remove(state);
    }

    /**
     * This method returns the Set of non final (accepting) states of the automaton.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of non final states
     */
    getNonFinalStates() {
        return (this.getStates()).minus(this.getFinalStates());
    }

    /**
     * This method is an alias of the getNonAcceptingStates method.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of non final states
     * @see Automaton#getNonAcceptingStates
     */
    getNonAcceptingStates(){
        return (this.getStates()).minus(this.getFinalStates());
    }
    /**
     * This method returns the Set of states of the automaton.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of non final states
     */
    getStates() {
        return this.states;
    }

    /**
     * This method sets the Set of states of the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} states The Set new Set of states of the automaton.
     * @param {Boolean} dontCopy If you don't want the function to copy the given Set of state, Set this to true; the Set will be used directly.
     */
    setStates(states, dontCopy) {
        if (states instanceof Set || states instanceof Array) {
            this.states = dontCopy ? toSet(states) : new Set(states);
        } else {
            throw new Error(("Automaton.setStates(): The given argument is not a Set."));
        }
    }

    /**
     * This method returns the Set of final (accepting) states of the automaton.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of final states of the automaton.
     * @see Automaton#setFinalStates
     * @see Automaton#setFinalState
     */
    getFinalStates() {
        return this.finalStates;
    }

    /**
     * This method is an alias of the getFinalStates method.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of final states of the automaton.
     * @see Automaton#getFinalStates
     */
    getAcceptingStates() {
        return this.finalStates;
    }

    /**
     * This method sets the Set of final (accepting) states of the automaton. Every other state is Set to non final.
     * @method
     * @memberof Automaton
     * @param {Set} states The new Set of final states of the automaton.
     * @param {Boolean} dontCopy If you don't want the function to copy the given Set of state, Set this to true; the Set will be used directly.
     * @see Automaton#getAcceptingStates
     */
    setFinalStates(states, dontCopy) {
        if (states instanceof Set || states instanceof Array) {
            this.finalStates = dontCopy ? toSet(states) : new Set(states);
        } else {
            throw new Error(("Automaton.setFinalStates(): The given argument is not a Set."));
        }
    }

    /**
     * This method is an alias of the setFinalStates method.
     * @method
     * @memberof Automaton
     * @see Automaton#setFinalStates
     */
    setAcceptingStates(states) {
        return this.setFinalState(states);
    }

    /**
     * This method sets the initial state of the automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The new initial state of the automaton.
     * @see Automaton#getInitialState
     */
    setInitialState(state) {
        this.states.add(state);
        this.qInit = state;
    }

    /**
     * This method returns the initial state of the automaton.
     * @method
     * @memberof Automaton
     * @returns {any} The initial state of the automaton.
     * @see Automaton#setInitialState
     */
    getInitialState(){
        return this.qInit;
    }

    /**
     * This method removes a state from the automaton.
     * @method
     * @memberof Automaton
     * @param {any} the state to remove
     * @see addState
     */
    removeState(state) {
        this.states.remove(state);
        this.finalStates.remove(state);
        if (this.qInit == state) this.qInit = null;
        var that = this;
        this.getTransitions().forEach(
            function (t) {
                if (t.startState === state || t.endState === state) {
                    that.removeTransition(t);
                }
            }
        );
    }

    /**
     * This method checks if the automaton has the state given in parameter.
     * @method
     * @memberof Automaton
     * @param {any} state The state to check
     * @returns {boolean} Returns true if the states is in the automaton, false otherwise.
     */
    hasState(state) {
        return this.states.has(state);
    }

    /**
     * This method checks if the state given in parameter is a final state of this automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The state to check
     * @returns {boolean} Returns true if the states is in the automaton, false otherwise.
     */
    isFinalState(state) {
        return this.finalStates.has(state);
    }

        /**
     * This method is an alias of the isFinalState method.
     * @method
     * @memberof Automaton
     * @returns {boolean} Returns true if the states is in the automaton, false otherwise.
     * @see Automaton#isFinalState
     */
    isAcceptingState(state) {
        return this.finalStates.has(state);
    }

    /**
     * This method adds a transition to the automaton. It can takes one or three arguments: a Transition object, or a state, a symbol and a state. This method automatically adds states and symbols which have not yet been added to the automaton.
     *
     * @method
     * @memberof Automaton
     * @example
     *    A = new Automaton();
     *    A.setInitialState(1);
     *    A.addFinalState(2);
     *    A.addTransition(1, "a", 2);
     *    var t = new Transition(1, epsilon, 2);
     *    A.addTransition(t);
     * @see Automaton#removeTransition
     * @see Automaton#hasTransition
     * @see Automaton#getTransitions
     * @see Automaton#getTransitionFunction
     * @see Transition
     */
    addTransition(t, t1, t2) {
        if (arguments.length > 1) {
            if (t1 instanceof Array){
                for (const symbol of t1){
                    if (is_epsilon(symbol)) this.addTransition(new Transition(t, "ε", t2));
                    else this.addTransition(new Transition(t, symbol, t2));
                }
                return;
            }
            else {
                if (is_epsilon(t1)) t1 = epsilon_toString();
                return this.addTransition(new Transition(t, t1, t2));
            }
        }
        this.states.add(t.startState);
        this.states.add(t.endState);
        this.addSymbol(t.symbol);

        if (t.symbol === 'Σ'){
            for (let symb of this.getAlphabet()){
                if (symb !=="ε") this.removeTransition(t.startState,symb,t.endState)
            }
            this.trans.add(t);
        }
        else {
            if (!this.hasTransition(t.startState,'Σ',t.endState)){
                this.trans.add(t);
            }
        }
    }

    /**
     * This function removes a transition to the automaton. It can takes one or three arguments: a Transition object, or a state, a symbol and a state. Don't forget to remove states or symbol that should also be removed, if necessary; this method does not do that automatically.
     *
     * @method
     * @memberof Automaton
     *
     * @see Automaton#addTransition
     * @see Automaton#hasTransition
     * @see Automaton#getTransitions
     * @see Automaton#getTransitionFunction
     * @see Transition
     * @see Automaton#removeSymbol
     * @see Automaton#removeState
     */
    removeTransition(t, t1, t2) {
        if (arguments.length > 1) {
            return this.removeTransition(new Transition(t, t1, t2));
        }

        this.trans.remove(t);
    }

    /**
     * This method checks if a transition exists in the automaton. It can takes one or three arguments: a Transition object, or a state, a symbol and a state.
     *
     * @method
     * @memberof Automaton
     *
     * @returns {boolean} Returns true if the transition exists, false otherwise
     * @see Automaton#addTransition
     * @see Automaton#removeTransition
     * @see Automaton#hasTransition
     * @see Automaton#getTransitions
     * @see Automaton#getTransitionFunction
     * @see Transition
     */
    hasTransition(t, t1, t2) {
        if (arguments.length > 1) {
            return this.hasTransition(new Transition(t, t1, t2));
        }

        return this.trans.has(t);
    }

    /**
     * This method returns the Set of transitions of the automaton.
     *
     * @method
     * @memberof Automaton
     *
     * @returns {Set} Returns the Set of transitions of the Automaton.
     * @see Automaton#addTransition
     * @see Automaton#removeTransition
     * @see Automaton#hasTransition
     * @see Automaton#getTransitionFunction
     * @see Transition
     */
    getTransitions() {
        return this.trans;
    }

    setTransitions(trans) {
        this.trans = new Set();
        if (trans) {
            for (let t of trans) {
                this.addTransition(t);
            }
        }
    }

    /**
     * This method returns the Set of symbols of the Automaton.
     *
     * @method
     * @memberof Automaton
     *
     * @returns {Set} Returns the alphabet of the automaton.
     *
     * @see Automaton#addSymbol
     * @see Automaton#removeSymbol
     */
    getAlphabet() {
        return this.Sigma;
    }

    /**
     * This method returns the transition function of the automaton. This function is such that:
     *  - f() returns the set of start states
     *  - f(startState) return the set of symbols such that one more (startState, symbol, endState) transitions exist(s)
     *  - f(startState, symbol) returns the set of states reachable with (startState, symbol). If determinizedFunction is true, return the only state reachable with (startState, symbol).
     *  - f(null, null, true) returns the set of endStates of all transitions.
     * @method
     * @memberof Automaton
     * @param {Boolean} determinizedFunction If the automaton is deterministic, makes the transition function easier to use (see description for more information). Leads to undetermined behavior in case the automaton is not deterministic.
     * @returns {Function} Returns a convenient function to manipulate transitions of the automaton.
     * @example
     *    var f = A.getTransitionFunction();
     *    f().forEach(function (startState) {
     *        f(startState).forEach(function (symbol) {
     *             f(startState, symbol).forReach(function (endState) {
     *                 console.log(startState, symbol, endState); // logs all the automaton's transition
     *             })
     *        });
     *    });
     * @see Automaton#addTransition
     * @see Automaton#removeTransition
     * @see Automaton#hasTransition
     * @see Transition
     */
    getTransitionFunction(determinizedFunction) {
        var transList = toArray(this.getTransitions()),
                transition,
                symbolsByState = [],
                startState,
                startStates = new Set(),
                endStates   = new Set(),
                endStatesByStartStateBySymbols = {},
                endStatesByStartStateEpsilon = {},
                symbol,
                t;

            for (t in transList) {
                transition = transList[t];
                startStates.add(transition.startState);
                endStates.add(transition.endState);
                startState = elementToString(transition.startState);
                if (!symbolsByState[startState]) {
                    symbolsByState[startState] = new Set();
                    endStatesByStartStateBySymbols[startState] = {};
                }

                if (is_epsilon(transition.symbol)) {
                    if (determinizedFunction) {
                        endStatesByStartStateEpsilon[startState] = transition.endState;
                    } else {
                        if (!endStatesByStartStateEpsilon[startState]) {
                            endStatesByStartStateEpsilon[startState] = new Set();
                        }
                        endStatesByStartStateEpsilon[startState].add(transition.endState);
                    }
                } else {
                    symbol = elementToString(transition.symbol);
                    if (determinizedFunction) {
                        endStatesByStartStateBySymbols[startState][symbol] = transition.endState;
                    } else {
                        if (!endStatesByStartStateBySymbols[startState][symbol]) {
                            endStatesByStartStateBySymbols[startState][symbol] = new Set();
                        }
                        endStatesByStartStateBySymbols[startState][symbol].add(transition.endState);
                    }
                }
                symbolsByState[startState].add(transition.symbol);
            }

            transList = null;
            return function (startState, symbol, getEndStates) {

                if (getEndStates) {
                    return endStates;
                }

                switch (arguments.length) {
                    case 0:
                        return startStates;
                    case 1:
                        return symbolsByState[elementToString(startState)] || new Set();
                    case 2:
                        var s;
                        if (is_epsilon(symbol)) {
                            s = endStatesByStartStateEpsilon[elementToString(startState)];
                            if (!determinizedFunction && s === undefined) {
                                return new Set();
                            }
                            return s;
                        }
                        if (determinizedFunction){
                            const a = (endStatesByStartStateBySymbols[elementToString(startState)] || [])[elementToString(symbol)];
                            const b = (endStatesByStartStateBySymbols[elementToString(startState)] || [])['Σ'];
                            s = a || b;
                        } else {
                            s = new Set();
                            s.unionInPlace(((endStatesByStartStateBySymbols[elementToString(startState)] || [])[elementToString(symbol)]) || new Set());
                            s.unionInPlace(((endStatesByStartStateBySymbols[elementToString(startState)] || [])['Σ']) || new Set());
                        }
                        if (!determinizedFunction && s === undefined) {
                            return new Set();
                        }
                        return s;
                }
            };
        }

    /**
     * This method sets the Set of symbols of the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} alphabet The Set of symbol to use.
     * @see Automaton#addAlphabet
     * @see Automaton#removeAlphabet
     * @see Automaton#addSymbol
     * @see Automaton#hasSymbol
     * @see Automaton#removeSymbol
     */
    setAlphabet(alphabet, byRef) {
        if (byRef) {
            if (alphabet instanceof Set) {
                this.Sigma = alphabet;
                return;
            }
            throw new Error("Automaton.setAlphabet(): The given argument is not a Set.");
        }

        this.Sigma = new Set(alphabet);
    }

    hasEpsilonTransitions() {
        for (let t of this.trans) {
            if (is_epsilon(t.symbol)) {
                return true;
            }
        }

        return false;
    }

    /**
     * This method adds a Set of symbols to the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} alphabet The Set of symbol to add to the current alphabet.
     * @see Automaton#setAlphabet
     * @see Automaton#removeAlphabet
     * @see Automaton#addSymbol
     * @see Automaton#hasSymbol
     * @see Automaton#removeSymbol
     */
    addAlphabet(alphabet) {
        this.Sigma.unionInPlace(alphabet);
    }

    /**
     * This method removes a Set of symbols from the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} alphabet The Set of symbol to remove from the current alphabet.
     * @see Automaton#addAlphabet
     * @see Automaton#setAlphabet
     * @see Automaton#addSymbol
     * @see Automaton#hasSymbol
     * @see Automaton#removeSymbol
     */
    removeAlphabet(alphabet) {
        this.Sigma.minusInPlace(alphabet);
    }

    /**
     * This method adds a symbol to the automaton.
     * @method
     * @memberof Automaton
     * @param {any} symbol The symbol to add
     * @see Automaton#removeSymbol
     * @see Automaton#hasSymbol
     * @see Automaton#addAlphabet
     * @see Automaton#setAlphabet
     * @see Automaton#removeAlphabet
     */
    addSymbol(symbol) {
        if (symbol !== epsilon) {
            this.Sigma.add(symbol);
        }
    }



    /**
     * This method tests whether a symbol belongs to the automaton.
     * @method
     * @memberof Automaton
     * @param {any} symbol The symbol to test
     * @returns {boolean} Returns true if the Automaton has the symbol, false otherwise.
     * @see Automaton#addAlphabet
     * @see Automaton#setAlphabet
     * @see Automaton#removeAlphabet
     * @see Automaton#addSymbol
     * @see Automaton#removeSymbol
    */
    hasSymbol(symbol) {
        return this.trans.has(symbol);
    }

    /**
     * This method removes a symbol from the automaton.
     * @method
     * @memberof Automaton
     * @param {any} symbol The symbol to remove
     * @see Automaton#addAlphabet
     * @see Automaton#setAlphabet
     * @see Automaton#removeAlphabet
     * @see Automaton#addSymbol
     * @see Automaton#hasSymbol
    */
    removeSymbol(symbol) {
        this.trans.add(symbol);
    }

    /**
     * This method returns a string representation of the automaton.
     * @method toString
     * @memberof Automaton
     * @note ATTENTION: This method is not stabilized yet. The string representation of the Automaton is still to be defined.
     * @returns {string} Returns the string representation of the Set.
     */
    toString() {
        return "Automaton(" + elementToString(this.states) + ", " + elementToString(this.Sigma) + ", " + elementToString(this.qInit) + ", " + elementToString(this.trans) + "," + elementToString(this.finalStates) + ")";
    }

    /**
     * This method sets the current state of the automaton
     * @method
     * @memberof Automaton
     * @param {any} state The state to make current
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentState
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentState
     * @see Automaton#removeCurrentStates
     * @see Automaton#getCurrentStates
    */
    setCurrentState(state) {
        this.lastTakenTransitions.clear();
        if (this.states.has(state)) {
            this.currentStates.clear();
            this.currentStates.add(state);
            this.currentStatesAddReachablesByEpsilon();
        }
    }

    /**
     * This method sets the current states of the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} states The Set of states to make current
     * @see Automaton#setCurrentState
     * @see Automaton#addCurrentState
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentState
     * @see Automaton#removeCurrentStates
     * @see Automaton#getCurrentStates
    */
    setCurrentStates(states) {
        this.lastTakenTransitions.clear();
        states = toSet(states);
        if (states.subsetOf(this.states)) {
            this.currentStates.clear();
            this.currentStates.unionInPlace(toSet(states));
            this.currentStatesAddReachablesByEpsilon();
        }
    }

    /**
     * This method make states of the automaton current.
     * @method
     * @memberof Automaton
     * @param {any} state The state to add to the Set of current states.
     * @see Automaton#setCurrentState
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentState
     * @see Automaton#removeCurrentStates
     * @see Automaton#getCurrentStates
    */
    addCurrentState(state) {
        if (this.states.has(state)) {
            this.currentStates.add(state);
            this.currentStatesAddReachablesByEpsilon();
        }
    }

    /**
     * This method remove a state from the Set of current states of the automaton.
     * @method
     * @memberof Automaton
     * @param {any} state The state to remove from the Set of current states.
     * @see Automaton#setCurrentState
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentState
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentStates
     * @see Automaton#getCurrentStates
    */
    removeCurrentState(state) {
        this.currentStates.remove(state);
        this.currentStatesAddReachablesByEpsilon();
    }

    /**
     * This method add a Set of states to the Set of current states of the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} states The Set of states to add to the Set of current states.
     * @see Automaton#setCurrentState
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentState
     * @see Automaton#removeCurrentState
     * @see Automaton#removeCurrentStates
     * @see Automaton#getCurrentStates
    */
    addCurrentStates(states) {
        this.currentStates.unionInPlace(states);
        this.currentStatesAddReachablesByEpsilon();
    }

    /**
     * This method add a Set of states to the Set of current states of the automaton.
     * @method
     * @memberof Automaton
     * @param {Set} states The Set of states to add to the Set of current states.
     * @see Automaton#setCurrentState
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentState
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentState
     * @see Automaton#getCurrentStates
    */
    removeCurrentStates(states) {
        this.currentStates.minusInPlace(states);
        this.currentStatesAddReachablesByEpsilon();
    }

    /**
     * This method returns the Set of current states of the automaton.
     * @method
     * @memberof Automaton
     * @return {Set} The Set of current states of the automaton.
     * @see Automaton#setCurrentState
     * @see Automaton#setCurrentStates
     * @see Automaton#addCurrentState
     * @see Automaton#addCurrentStates
     * @see Automaton#removeCurrentState
     * @see Automaton#removeCurrentStates
    */
    getCurrentStates() {
        return this.currentStates;
    }

    /**
     * This methods looks at current states and transitions of the Automaton to add all states reachable with epsilon to the current states.
     * @method
     * @memberof Automaton
     * @param {Function} [transitionFunction] The transition function, as given by the getTransitionFunction() method
     * @param {Set} [visited] States that were already visited by the function.
     */
    currentStatesAddReachablesByEpsilon(transitionFunction, visited) {
        var cs   = toArray(this.currentStates),
            cont = false, // we continue if we added states
            th   = this;

        if (!visited) {
            visited = new Set();
        }

        if (!transitionFunction) {
            transitionFunction = this.getTransitionFunction();
        }

        var i;

        function browseState(state) {
            if (!visited.has(state)) {
                th.currentStates.add(state);
                th.lastTakenTransitions.add(new Transition(cs[i], epsilon, state));
                cont = true;
            }
        }

        for (i = 0; i < cs.length; ++i) {
            if (!visited.has(cs[i])) {
                visited.add(cs[i]);
                transitionFunction(cs[i], epsilon).forEach(browseState);
            }
        }

        if (cont) {
            this.currentStatesAddReachablesByEpsilon(transitionFunction, visited);
        }
    }

        /**
     * This methods returns the set of successors of a state. Its behavior is well defined only on determinized automata.
     *
     * @method
     * @memberof Automaton
     * @param {State} [state] The state from which the successors are to get
     * @param {Symbol} [symbol] If given, restrain successors to a symbol. If not given, consider all symbols.
     */
    getSuccessors(state, symbol) {
        var successors = new Set();
        var allSymbols = arguments.length === 1;

        this.trans.forEach(
            function (t) {
                if (t.startState === state) {
                    if (allSymbols || t.symbol === symbol) {
                        successors.add(t.endState);
                    }
                }
            }
        );

        return successors;
    }

    getReachable(state, visited) {
        if (state === undefined) {
            state = this.getInitialState();
        }

        if (!visited) {
            visited = new Set();
        }

        var that = this;

        this.getSuccessors(state).forEach(
            function (s) {
                if (s !== state && !visited.has(s)) {
                    visited.add(s);
                    visited.unionInPlace(that.getReachable(s, visited));
                }
            }
        );

        return visited;
    }


    /**
     * This methods looks at current states and transitions of the Automaton to replace current states by all states reachable with the given symbol.
     * @method
     * @memberof Automaton
     * @throws {Error} Throws an error if epsilon is given as the symbol.
     * @param {any} symbol The symbol to "eat"
     * @param {Function} [transitionFunction] The transition function, as given by the getTransitionFunction() method
     * @param {boolean} [dontEraseTakenTransitions] dontEraseTakenTransitions If true, don't reinitialize the Set of last taken transitions, just append newly taken transition to it.
     * @see Automaton#runWord
     * @see Automaton#acceptedWord
     * @see Automaton#getCurrentStates
     * @see Automaton#getLastTakenTransitions
     */
    runSymbol(symbol, transitionFunction, dontEraseTakenTransitions) {
        if (is_epsilon(symbol)) {
            throw new Error("Automaton.runSymbol(): epsilon is forbidden.");
        }

        if (!this.Sigma.has(symbol) && !this.Sigma.has('Σ')) {
            this.lastTakenTransitions.clear();
            this.currentStates.clear();
            return false;
        }

        if (!transitionFunction) {
            transitionFunction = this.getTransitionFunction();
        }

        if (!dontEraseTakenTransitions) {
            this.lastTakenTransitions.clear();
        }

        var cs = toArray(this.currentStates),
            th = this;

        var i;

        function addState(state) {
            th.currentStates.add(state);
            th.lastTakenTransitions.add(new Transition(cs[i], symbol, state));
        }

        for (i = 0; i < cs.length; ++i) {
            this.currentStates.remove(cs[i]);
        }

        for (i = 0; i < cs.length; ++i) {
            transitionFunction(cs[i], symbol).forEach(addState);
        }

        this.currentStatesAddReachablesByEpsilon(transitionFunction);
    }

    /**
     * This method runs each symbol of the list of symbol given. See the description of the runSymbol for more information. Don't forget to Set the current state to the initial state if you need it.
     * @method
     * @memberof Automaton
     * @throws {Error} Throws an error if epsilon is given as a symbol.
     * @param {Array} The list of symbols to run
     * @see Automaton#runSymbol
     * @see Automaton#acceptedWord
     * @see Automaton#getCurrentStates
    */
    runWord(symbols) {
        var i, transitionFunction = this.getTransitionFunction();
        for (i in symbols) {
            this.runSymbol(symbols[i], transitionFunction);
        }
    }

    /**
     * This method runs a word from the initial state and checks if this word is accepted by the automaton. It takes care to restore current states and last taken transitions after the run.
     * @method
     * @memberof Automaton
     * @throws {Error} Throws an error if epsilon is given as a symbol.
     * @param {Array} The list of symbols to run
     * @see Automaton#runSymbol
     * @see Automaton#runWord
    */
    acceptedWord(symbols) {
        var states      = toArray(this.getCurrentStates()),
            transitions = new Set(this.getLastTakenTransitions());

        this.setCurrentState(this.getInitialState());
        this.runWord(symbols);
        var accepted = (this.currentStates.inter(this.finalStates)).card() !== 0;
        this.setCurrentStates(states);
        this.lastTakenTransitions = transitions;
        return accepted;
    }

    /**
     * This method returns the Set of transitions that were taken while running on the last symbol. See the runSymbol method for more information.
     * @method
     * @memberof Automaton
     * @returns {Set} The Set of last taken transitions
     * @see Automaton#runSymbol
     * @see Automaton#getCurrentStates
    */
    getLastTakenTransitions() {
        return this.lastTakenTransitions;
    }

    copy() {
        return new Automaton(this.states,this.Sigma,this.qInit,this.trans,this.finalStates);
    }
}

/**
 * A class to manipulate transitions of automata in Javascript.

    * @class
    * @alias Transition
    */
class Transition {
    constructor(startState, symbol, endState){
        this.startState = startState;
        this.symbol     = symbol;
        this.endState   = endState;
    }   
    toString() {
        return "Transition(" + elementToString(this.startState) + ", " + elementToString(this.symbol) + ", " + elementToString(this.endState) + ")";
    }
}

exports.Automaton = Automaton;
exports.Transition = Transition;
