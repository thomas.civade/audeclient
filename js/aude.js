/*
    Copyright (C) 2023 Thomas Civade (UGA)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
*/

/**
 * Utils for utilisation of Pkg class extends in automaton
 *
 * @module aude
 */

'use strict';

/**
 * minus(set1,set2) method to create the set resulting of set1 - set in set theory:
 * 
 * @method
 * 
 * @param {Set} set1 a set of items
 * @param {Set} set2 a set of items
 * 
 * @return {Set}
 */

function minus (set1, set2) {
    return new Set([...set1].filter(element => !set2.has(element)));
}
    
function getNextValue(s, j, len, map) {
    if (len === undefined) {
        len = s.length;
    }

    while (j < len && (!s[j].trim() || s[j] === ",")) {
        ++j;
    }

    var j0 = j, lastIndex, end, closed, nextValue;

    if (s[j] === "\"" || s[j] === "'") {
        end = s[j++];
        while (j < len && s[j] !== end) {
            if (s[j] === "\\") {
                ++j;
            }
            ++j;
        }

        lastIndex = j + 1;
        return {
            value: JSON.parse(
                end === "\""
                    ? s.substring(j0, j + 1)
                    : "\"" + s.substring(j0 + 1, j).replace(/"/g, "\\\"") + "\""
            ),
            lastIndex: lastIndex
        };
    }

    if (s[j] === "{") {
        var key, set = new Set();
        ++j;
        closed = false;
        while (j < len) {
            while (j < len && (!s[j].trim() || s[j] === ",")) {
                ++j;
            }

            if (s[j] === "}") {
                lastIndex = j + 1;
                closed = true;
                j = lastIndex;
                break;
            }

            nextValue = getNextValue(s, j, len, map);

            if (j === nextValue.lastIndex) {
                throw new Error(_("Value is malformed."));
            }

            j = nextValue.lastIndex;

            if (s[j] === ":") {
                if (set instanceof Set) {
                    if (set.card() === 0) {
                        set = {};
                    } else {
                        throw new Error(_("Value is malformed."));
                    }
                }
                ++j;

                key = nextValue.value;
                nextValue = getNextValue(s, j, len, map);
                set[key] = nextValue.value;
            } else if (set instanceof Set) {
                set.add(nextValue.value);
            } else {
                throw new Error(_("Value is malformed."));
            }

            j = nextValue.lastIndex;
        }

        if (!closed) {
            throw new Error(_("Value is malformed."));
        }

        return {
            value:set,
            lastIndex:lastIndex
        };
    }

    if (s[j] === "(" || s[j] === "[") {
        end = s[j] === "(" ? ")" : "]";
        var tuple = (end === ")") ? new Tuple() : [];
        ++j;
        closed = false;
        while (j < len) {
            while (j < len && (!s[j].trim() || s[j] === ",")) {
                ++j;
            }

            if (s[j] === end) {
                lastIndex = j + 1;
                j = lastIndex;
                closed = true;
                break;
            }

            nextValue = getNextValue(s, j, len, map);

            if (j === nextValue.lastIndex) {
                throw new Error(_("Value is malformed."));
            }

            tuple.push(nextValue.value);
            j = nextValue.lastIndex;
        }

        if (!closed) {
            throw new Error(_("Value is malformed."));
        }

        return {
            value:tuple,
            lastIndex:lastIndex
        };
    }

    while (j < len && s[j].trim() && ":(,})]".indexOf(s[j]) === -1) {
        ++j;
    }

    var valName = s.substring(j0, j).trim();

    if (s[j] === "(") {
        var values = [];

        if (/^[a-zA-Z]+$/.test(valName)) {
            if (typeof that[valName] !== "function") {
                throw new Error(_("Constructor name in value refers to unkown class."));
            }
            ++j;
            while (j < len && s[j] !== ")") {
                nextValue = getNextValue(s, j, len, map);
                j = nextValue.lastIndex;
                values.push(nextValue.value);
            }
        }

        if (valName === "Date") {
            return {
                value : new Date(values[0] || null),
                lastIndex: j + 1
            };
        }

        if (valName === "Object") {
            return {
                value : values[0] || null, // FIXME check correction
                lastIndex: j + 1
            };
        }

        var F = function (){return;};

        F.prototype = that[valName].prototype;
        var v = new F();
        that[valName].apply(v, values);

        return {
            value:v,
            lastIndex:j + 1
        };
    }

    switch (valName) {
    case "true":
        return {
            value:true,
            lastIndex:j
        };
    case "false":
        return {
            value:false,
            lastIndex:j
        };
    case "null":
        return {
            value:null,
            lastIndex:j
        };
    case "undefined":
        return {
            value:undefined,
            lastIndex:j
        };
    case "NaN":
        return {
            value:NaN,
            lastIndex:j
        };
    case "Infinity":
    case "+Infinity":
        return {
            value:Infinity,
            lastIndex:j
        };
    case "-Infinity":
        return {
            value:-Infinity,
            lastIndex:j
        };
    default:
        var d = parseFloat(valName);
        if (d.toString() === valName) {
            return {
                value:d,
                lastIndex:j
            };
        }
    }

    return {
        value:typeof map === "object" && map.hasOwnProperty(valName) ? map[valName] : valName,
        lastIndex:j
    };
}


function getValue(s, map) {
    s = s.trim();
    var len = s.length,
            nextValue = getNextValue(s, 0, len, map);
    if (nextValue.lastIndex === len) {
        return nextValue.value;
    } else
    throw new Error(_("Value is malformed."));
}

/**
 * toArray(l) transfrom the object l into an array with the same values contained
 * 
 * @method
 * 
 * @param {Object} l an object containing values
 * 
 * @returns {Array}
 */
function toArray(l) {
    if (l instanceof Set || l instanceof Tuple) {
        return l.getList();
    } else if (l instanceof Array) {
        return l;
    }

    throw new Error(_("Cannot make an array from arbitrary type"));
}

/**
 * toSet(iterable) transform an iterable Object into a set
 * 
 * @method
 * 
 * @param {Object} iterable an iterable Object
 * 
 * @returns {Set}
 */
function toSet (iterable) {
    return (
        iterable instanceof Set
            ? iterable
            : new Set(iterable)
    );
}


module.exports = {
    toArray,
    toSet,
    getNextValue,
    getValue,
};
