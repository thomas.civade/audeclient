/*
    Copyright (c) 2013, Raphaël Jakse (Université Joseph Fourier)
    All rights reserved.
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Université Joseph Fourier nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @author  Raphaël Jakse
 * @author Thomas Civade (doc)
 * @file This is a library for manipulating mathematical sets.
 * @note Any function of this package that takes a Set also accepts any object that can be turned into a set (e.g. Object, Array variables).
 * @version 0.1a
 */

/**
 * This method return a string equivalent of the entry array
 *
 * @method
 * 
 * @param {Array} l the array to transform
 */
function listToString(l) {
    let res = "";

    for (let v of l) {
        if (res) {
            res += ",";
        }

        res += elementToString(v);
    }

    if (l instanceof PkgTuple) {
        if (l.length === 1) {
            return res;
        }

        return "(" + res + ")";
    }

    return "[" + res + "]";
}

/**
 * This method return a string equivalent of any on iterable entry
 *
 * @method
 * 
 * @param {any} e the element to transform
 * @param {Map} map a map of element to string
 * 
 * @returns {String}
 */
function elementToString(e, map) {
    if (typeof e === "number" && isNaN(e)) {
        return "NaN";
    }

    switch (e) {
    case undefined:
        return "undefined";
    case null:
        return "null";
    case -Infinity:
        return "-Infinity";
    case Infinity:
        return "Infinity";
    default:
        if (e instanceof Array || e instanceof PkgTuple) {
            return listToString(e);
        }

        if (typeof e === "string") {
            if (!e.length || /["'\\{\[\]}\(\),\s]/.test(e) || parseFloat(e).toString() === e || (typeof map === "object" && map.hasOwnProperty(e))) {
                e = JSON.stringify(e);
            }
            return e.toString();
        }

        if (e instanceof Date) {
            return "Date(\"" + e.toString() + "\")";
        }


        if (typeof e === "object") {
            if (e instanceof Object) {
                if (e.serializeElement) {
                    return e.serializeElement();
                }

                if (e.toJSON) {
                    return JSON.stringify(e);
                }
            }

            var i, res = "", keys = Object.keys(e).sort();
            for (i in keys) {
                if (res) {
                    res += ",";
                } else {
                    res = "{";
                }

                res += JSON.stringify(keys[i]) + ":" + elementToString(e[keys[i]]);
            }
            return res ? res + "}" : "Object()";
        }

        return e.toString();
    }
}

/**
 * An extended class to manipulate Map using elementToString function
 * 
 * @extends {Map}
 * @alias PkgMap
*/
class PkgMap extends Map {
    constructor(keyValueIterable) {
        super();

        this.keyStringToKeyObject = Object.create(null);

        if (keyValueIterable) {
            for (let [k, v] of keyValueIterable) {
                this.set(k, v);
            }
        }
    }
    /**
     * This method adds a key-value to the map.
     * @method
     * @memberof PkgMap
     * @param {any} key The key added (must be hashable)
     * @param {any} value
     */
    set(key, value) {
        let keyString = elementToString(key);

        if (keyString in this.keyStringToKeyObject) {
            key = this.keyStringToKeyObject[keyString];
        } else {
            this.keyStringToKeyObject[keyString] = key;
        }

        super.set(key, value);
    }

    /**
     * This method return the value corresponding to the key. 
     * @method
     * @memberof PkgMap
     * @param {any} key 
     */
    get(key) {
        let keyString = elementToString(key);

        if (keyString in this.keyStringToKeyObject) {
            return super.get(this.keyStringToKeyObject[keyString]);
        }

        return undefined;
    }

    /**
     * This method return if he map has 'key' in his key list
     * @method
     * @memberof PkgMap
     * @param {any} key 
     */
    has(key) {
        return elementToString(key) in this.keyStringToKeyObject
    }

    /**
     * This method remove the key-value of the map based on the key
     * @method
     * @memberof PkgMap
     * @param {any} key 
     */
    delete(key) {
        let keyString = elementToString(key);

        if (keyString in this.keyStringToKeyObject) {
            key = this.keyStringToKeyObject[keyString];
            delete this.keyStringToKeyObject[keyString];
            super.delete(key);
            return true;
        }

        return false;
    }


    /**
     * This method remove all key and value from the map
     * @method
     * @memberof PkgMap
     * @param {any} key 
     */
    clear() {
        this.keyStringToKeyObject = Object.create(null);
        super.clear();
    }
}

/**
 * An extended class to manipulate Set as in mathematic set theory
 * 
 * @extends {Set}
 * @alias PkgSet
*/
class PkgSet extends Set {
    constructor(values) {
        super();
        this.valStringToValObject = Object.create(null);

        if (values) {
            for (let v of values) {
                this.add(v);
            }
        }
    }

    /**
     * This method add an elmeent into the set
     * @method
     * @memberof PkgSet
     * @param {any} v 
     */
    add(v) {
        let valString = elementToString(v);

        if (valString in this.valStringToValObject) {
            return;
        }

        this.valStringToValObject[valString] = v;

        super.add(v);
    }

    /**
     * This method delete an element from the set
     * @method
     * @memberof PkgSet
     * @param {any} v 
     */
    delete(v) {
        let valString = elementToString(v);

        if (!(valString in this.valStringToValObject)) {
            return false;
        }

        super.delete(this.valStringToValObject[valString]);
        delete this.valStringToValObject[valString];

        return true;
    }
    
    /**
     * This method is an alias of te delete method.
     * @see PkgSet#delete
     */
    remove(v) {
        let valString = elementToString(v);

        if (!(valString in this.valStringToValObject)) {
            return false;
        }

        super.delete(this.valStringToValObject[valString]);
        delete this.valStringToValObject[valString];

        return true;
    }

    /**
     * This method remove all content of a set
     * @method
     * @memberof PkgSet
     */
    clear() {
        this.valStringToValObject = Object.create(null);
        super.clear();
    }

    /**
     * This method test if the set has the element v
     * @method
     * @memberof PkgSet
     * 
     * @param {any} v
     * 
     * @returns {Boolean}
     */
    has(v) {
        return elementToString(v) in this.valStringToValObject;
    }

    /**
     * This method realize the mathematical union of 2 set and uodate the set values
     * @method
     * @memberof PkgSet
     * 
     * @param {PkgSet} set the second set of the union (non-modified)
     * 
     * @returns {PkgSet} the set himself
     */
    unionInPlace(set) {
        for (let v of set) {
            this.add(v);
        }

        return this;
    }

    /**
     * This method realize the mathematical intersection of 2 set and uodate the set values
     * @method
     * @memberof PkgSet
     * 
     * @param {any} set the second set/PkgSet of the intersection (non-modified)
     * 
     * @returns {PkgSet} the set himself
     */
    interInPlace(set) {
        if (!(set instanceof PkgSet)) {
            set = new PkgSet(set);
        }

        for (let v of this) {
            if (!set.has(v)) {
                this.delete(v);
            }
        }

        return this;
    }

    /**
     * This method realize the mathematical minus of 2 set and uodate the set values
     * @method
     * @memberof PkgSet
     * 
     * @param {PkgSet} set the second set of the substraction 
     * 
     * @returns {PkgSet} the set himself
     */
    minusInPlace(set) {
        for (let v of set) {
            this.delete(v);
        }

        return this;
    }

    /**
     * This method is an alias of the minusInPlace method but it create a copy of the original set.
     * @see PkgSet#minusInPlace
     */
    minus(set) {
        return new PkgSet(this).minusInPlace(set);
    }

    /**
     * This method is an alias of the interInPlace method but it create a copy of the original set.
     * @see PkgSet#interInPlace
     */
    inter(set) {
        return new PkgSet(this).interInPlace(set);
    }

    /**
     * This method is an alias of the unionInPlace method but it create a copy of the original set.
     * @see PkgSet#unionInPlace
     */
    union(set) {
        return new PkgSet(this).unionInPlace(set);
    }

    /**
     * This method return a PkgSet of PkgTuple of all association of element in 2 PkgSet
     * @method
     * @memberof PkgSet
     * 
     * @param {PkgSet} set the second set
     * 
     * @returns {PkgSet} the set of tuples
     */
    cross(set) {
        var res = new PkgSet();

        for (let v1 of this) {
            for (let v2 of set) {
                res.add((new PkgTuple()).fromList([v1, v2]));
            }
        }

        return res;
    }

    /**
     * This method test if this set is a subset of the argument set
     * @method
     * @memberof PkgSet
     * 
     * @param {any} set the second set
     * 
     * @returns {Boolean} true if this is a subset of set, false otherwise
     */
    subsetOf (set) {
        if (!(set instanceof PkgSet)) {
            set = new PkgSet(set);
        }

        for (let v of this) {
            if (!set.has(v)) {
                return false;
            }
        }

        return true;
    }

    /**
     * This method return a set of all element present in at least 1 set but not 2
     * @method
     * @memberof PkgSet
     * 
     * @param {any} set the second set (PkgSet or iterable object)
     * 
     * @returns {Set} set of element from the 2 initial PkgSet
     */
    symDiff(set) {
        let r = new Set();

        if (!(set instanceof PkgSet)) {
            set = new PkgSet(set);
        }

        for (let v of this) {
            if (!set.has(v)) {
                r.add(v);
            }
        }

        for (let v of set) {
            if (!this.has(v)) {
                r.add(v);
            }
        }

        return r;
    }

    /**
     * This method create a copy of this set and them add multiple element into whithout modifying the initial one
     * @method
     * @memberof PkgSet
     * 
     * @argument {any}
     * @see PkgSet#unionInPlace
     * 
     * @returns {PkgSet}
     */
    plus(...args) {
        let r = new PkgSet(this);
        r.unionInPlace(args);
        return r;
    }

     /**
     * This method create a set containing the actual set 
     * @method
     * @memberof PkgSet
     * 
     * @returns {PkgSet}
     */
    powerset() {
        if (this.size === 0) {
            return new PkgSet([new PkgSet()]);
        }

        let lastE, Complement = new PkgSet();

        for (let v of this) {
            Complement.add(v);
            lastE = v;
        }

        Complement.remove(lastE);

        let PCompl = Complement.powerset();
        let U = new Set();

        for (let underset of PCompl) {
            U.add(underset.plus(lastE));
        }

        return PCompl.union(U);
    }

    /**
     * This method cretae an array from this set
     * @method
     * @memberof PkgSet
     * 
     * @returns {Array}
     */
    getList() {
        return Array.from(this);
    }

    /**
     * This method return the sorted array with the same value of this set
     * @method
     * @memberof PkgSet
     * 
     * @returns {Array}
     */
    getSortedList() {
        return Array.from(this).sort(
            function sort(a, b) {
                // FIXME continue
                if (typeof a === "number") {
                    if (typeof b === "number") {
                        return a - b;
                    }
                    return 1;
                }

                if (typeof b === "number") {
                    return -1;
                }

                return elementToString(a) > elementToString(b);
            }
        );
    }

    /**
     * This method return the string of all element of this set
     * @method
     * @memberof PkgSet
     * 
     * @see PkgSet#getSortedList
     * @see elementToString
     * 
     * @returns {String}
     */
    toString() {
        return this.size ? "{" + this.getSortedList().map(elementToString).join(",") + "}" : "∅";
    }

     /**
     * This method apply a func who return a boolean on each element of the set 
     * and return true if they all validate this function
     * @method
     * @memberof PkgSet
     * 
     * @returns {Boolean}
     */
    every(func) {
        for (v of this) {
            if (!func(this.l[i])) {
                return false;
            }
        }

        return true;
    }

    /**
     * This method apply a func who return a boolean on each element of the set  
     * and return true if at least one of them validate this function
     * @method
     * @memberof PkgSet
     * 
     * @returns {Boolean}
     */
    some(func) {
        for (v of this) {
            if (func(this.l[i])) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method create a deep copy of this set
     * @method
     * @memberof PkgSet
     * 
     * @returns {PkgSet}
     */
    copy() {
        return new PkgSet(this);
    }

    /**
     * This method return the first item of this set
     * @method
     * @memberof PkgSet
     * 
     * @returns {any} 
     */
    getItem() {
        for (let v of this) {
            return v;
        }
    }

    /**
     * This method return the size of this set
     * @method
     * @memberof PkgSet
     * 
     * @returns {Number} 
     */
    card() {
        return this.size;
    }
}

/**
 * A class to manipulate Tuple of couple of values
 * 
 * @class
 * @alias PkgTuple
*/
class PkgTuple  {
    constructor() {
      this.length = 0;
      this.blockCheckCoupleToTuple = false;
    }
    
    /**
     * This method create a tuple from a list
     * @method
     * @memberof PkgTuple
     * 
     * @see PkgTuple#push
     * @see PkgTuple#checkCoupleToTuple
     * 
     * @param {Array} l 
     * 
     * @returns {PkgTuple} 
     */
    fromList(l) {
      this.blockCheckCoupleToTuple = true;
      for (let i = 0; i < l.length; ++i) {
        this.push(l[i]);
      }
      this.blockCheckCoupleToTuple = false;
      this.checkCoupleToTuple();
      return this;
    }
  
    /**
     * This method flat an iterable object to add it into this tuple 
     * @method
     * @memberof PkgTuple
     * 
     * @see PkgTuple#add
     * @see PkgTuple#setItem
     * 
     * @param {any} l 
     * 
     * @returns {PkgTuple} return himself
     */
    flattenList(l) {
      let cur = 0;
      const th = this;
  
      function add(e) {
        if (e instanceof PkgTuple || e instanceof Array) {
          for (let i = 0; i < e.length; ++i) {
            add(e[i]);
          }
        } else {
          th.setItem(cur, e);
          ++cur;
        }
      }
  
      add(l);
      return this;
    }
  
    /**
     * This method return the item at the index i
     * @method
     * @memberof PkgTuple
     * 
     * @param {Number} i 
     * 
     * @returns {any} 
     */
    item(i) {
      return this[i];
    }
  
    /**
     * This method set the item e at the index i
     * @method
     * @memberof PkgTuple
     * 
     * @param {Number} i 
     * @param {any} e 
     * @param {Boolean} [noCheckLength]
     * 
     * @returns {PkgTuple} return himself
     */
    setItem(i, e, noCheckLength) {
      if (!noCheckLength && this.length <= i) {
        while (this.length <= i) {
          this.length = i + 1;
        }
      }
  
      Object.defineProperty(this, i, {
        enumerable: true,
        configurable: true,
        get() {
          return e;
        },
        set(nv) {
          e = nv;
          this.checkCoupleToTuple();
        },
      });
  
      this.checkCoupleToTuple();
      return this;
    }
  
    /**
     * This method add the new item e
     * @method
     * @memberof PkgTuple
     * 
     * @see PkgTuple#setItem
     * @see PkgTuple#checkCoupleToTuple
     * 
     * @param {any} e 
     * 
     * @returns {PkgTuple} return himself
     */
    push(e) {
      this.setItem(this.length, e);
      this.checkCoupleToTuple();
      return this;
    }
  
    /**
     * This method verify if elements of the tuple form a couple
     * @method
     * @memberof PkgTuple
     * 
     * @see PkgTuple#setItem
     * @see PkgTuple#push
     * 
     */
    checkCoupleToTuple() {
      if (!this.blockCheckCoupleToTuple) {
        this.blockCheckCoupleToTuple = true;
        if (this.length !== 2 || !(this[0] instanceof PkgTuple)) {
          return;
        }
  
        const lastItem = this[1];
        const t = this[0];
        this.length = 0;
  
        for (let i = 0; i < t.length; ++i) {
          this.setItem(i, t[i]);
        }
  
        this.push(lastItem);
        delete this.blockCheckCoupleToTuple;
        this.checkCoupleToTuple();
      }
    }
  
    /**
     * This method return the array form of this tuple
     * @method
     * @memberof PkgTuple
     * 
     * @returns {Array}
     */
    asCouple() {
      switch (this.length) {
        case 0:
          return [];
        case 1:
          return [this[0]];
        case 2:
          return [this[0], this[1]];
        default:
          return [
            PkgTuple(Array.prototype.slice.call(this, 0, this.length - 1)),
            this[this.length - 1],
          ];
      }
    }
    
    /**
     * This method return the array form of this tuple
     * @method
     * @memberof PkgTuple
     * 
     * @returns {Array}
     */
    getList() {
      const l = [];
      l.length = this.length;
      for (let i = 0; i < this.length; ++i) {
        l[i] = this[i];
      }
      return l;
    }
  
    /**
     * This method return the string form of this tuple using ',' to separate element
     * @method
     * @memberof PkgTuple
     * 
     * @returns {String}
     */
    toString() {
        return this.getList().join(', ');
    }
  
    *[Symbol.iterator]() {
      for (let i = 0; i < this.length; i++) {
        yield this[i];
      }
    }
  }

Tuple = PkgTuple;
Set = PkgSet;
Map = PkgMap;

/**
 * This method transform in iterable element into a PkgSet, else return null
 * @method
 * 
 * @param {Iterable} iterable
 * 
 * @returns {PkgSet}
 */
toSet = function (iterable) {
    if (!(iterable instanceof PkgSet)) {
        return new PkgSet(iterable);
    }
}


module.exports = {
    Map,
    Tuple,
    Set,
    elementToString,
    listToString
};