/*
    Copyright (C) 2023 Thomas Civade (UGA)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
*/

'use strict';

/**
 * @author  Thomas Civade
 * @file This is a library for applying algorithms and store/load automatons using the aude api.
 * @note Any function of this package that get token do a call to the aude api need to be registered on aude.api.fr
 */

const {automaton2json, json2automaton} = require("./automaton2json");

// API url
const url = "http://localhost:3000";
// API token

/**
 * Verify the response status is 200 (OK) else return false
 *
 * @method
 * 
 * @param {JSON} response the JSON repose of the request
 * 
 * @returns {boolean} true : good response; false : error
 */
function verify_response(response){
    if (response.status !== 200){
        console.log("Error when calling the API :",response.status);
        return false;
    } else {
        return true;
    }
}

/**
 * Method to transform an automaton into a regular expression
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {String} the regex who recognize the same language as the automaton
 */
async function automaton2regex(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/regex", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.regex;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to transform an automaton into a unix regular expression
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {String} the unix regex who recognize the same language as the automaton
 */
async function automaton2unixregex(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/unixregex", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.regex;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to perform a BFS on an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Array} array of states
 */
async function BFS(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/bfs", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.states;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the completed version of an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} completed automaton
 */
async function complete(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/complete", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if our automaton is completed
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean} 
 */
async function isCompleted(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/iscompleted", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.iscompleted;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}


/**
 * Method to complement an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} the complement automaton
 */
async function complement(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/complement", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to concatenate 2 automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton_A
 * @param {Automaton} automaton_B 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} the concatenate A-B automaton
 */
async function concatenate(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/concatenate", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to list all coreachable states
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Array} array of coreachable states
 */
async function coreachableStates(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/coreachablestates", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.states;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if all states of an automaton are coreachable
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function isCoreachable(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/iscoreachable", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.iscoreachable;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to perform a DFS on an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Array} array of state
 */ 
async function DFS(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/dfs", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.states;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to determinize an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} determinized automaton
 */
async function determinize(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/complement", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if an automaton is determinized
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function isDeterminized(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/isdeterminized", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.isdeterminized;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the automaton resulting of the difference of 2 automatons
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton}
 */
async function difference(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/difference", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if an automaton rocognize only an empty language
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function emptyLanguage(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/emptylanguage", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.emptylanguage;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to elimine epsilon transition from an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function epsElimination(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/epselim", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test the equivalence of the language recognize by 2 automatons
 *
 * @method
 * @async@async
 * @param {Automaton} automaton_A 
 * @param {Automaton} automaton_B
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function equivalence(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/areequivalent", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.areequivalent;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if the automate_A is include in the automate_B
 *
 * @method
 * @async
 * @param {Automaton} automaton_A 
 * @param {Automaton} automaton_B
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function include(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/inclusion", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
       
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.isinclude;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if an automaton rocognize an infinite language
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function infiniteLanguage(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/infinitelanguage", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.infinitelanguage;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the minimized automaton equivalent of the entry one
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} the minimized automaton
 */
async function minimize(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/minimize", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to test if an automaton is minimized
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function isMinimized(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/isminimized", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.isminimized;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the automaton who recognize the mirrored lagnuage of the entry one. 
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} the mirror automaton
 */
async function mirror(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/mirror", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to normalize an automaton
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Autoamton} the normalized automaton
 */
async function normalize(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/normalize", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the automaton resulting of the intersection of 2 automatons
 *
 * @method
 * @async
 * @param {Automaton} automaton_A 
 * @param {Automaton} automaton_B 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton}
 */
async function product(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/product", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to list all reachable states
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Array} araay of reachable states
 */
async function reachableStates(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/reachablestates", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.states;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}


/**
 * Method to test if all states of an automaton are reachable
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} token the authorization token
 * 
 * @returns {Boolean}
 */
async function isReachable(automaton,token){
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/isreachable", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return data.isreachable;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to transform a regex into an automaton
 *
 * @method
 * @async
 * @param {String} regex 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} an automaton who recognize the same language as the regex
 */
async function regex2automaton(regex,token){
    const response = await fetch(url + "/getregex", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "regex": regex
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to get the automaton resulting of the union of 2 automatons
 * @method
 * @async
 * @param {Automaton} automaton_A
 * @param {Automaton} automaton_B 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} automaton who recognize the union of the language of the 2 entry automatons
 */
async function unionAutomaton(automaton_A, automaton_B, token) {
    const JSONautomatonA = automaton2json(automaton_A);
    const JSONautomatonB = automaton2json(automaton_B);
    const response = await fetch(url + "/union", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton1: JSONautomatonA, 
            automaton2: JSONautomatonB
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to transform a unix regex into an automaton
 *
 * @method
 * @async
 * @param {String} regex 
 * @param {String} token the authorization token
 * 
 * @returns {Automaton} the corresponding Automaton
 */
async function unixregex2automaton(regex,token){
    const response = await fetch(url + "/getunixregex", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "regex": regex
        })
    });
    const data = await response.json();
    if (verify_response(response)){
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to store an automaton in the DB
 *
 * @method
 * @async
 * @param {Automaton} automaton 
 * @param {String} title title of the automaton stored
 * @param {String} token the authorization token
 * 
 * @returns {Boolean} true if the automaton is store, else otherwise
 */
async function store(automaton, title, token) {
    const JSONautomaton = automaton2json(automaton);
    const response = await fetch(url + "/store", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            automaton: JSONautomaton, 
            title: title
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.added;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to search titles depending of filters in your stored automatons
 *
 * @method
 * @async
 * @param {String} token the authorization token
 * @param {Boolean} [isCompleted] add a filter on if the automaton is complete
 * @param {Boolean} [isReachable] add a filter on if the automaton is reachable
 * @param {Boolean} [isCoreachable] add a filter on if the automaton is coreachable
 * @param {Boolean} [isDeterminized] add a filter on if the automaton is determinize
 * @param {Boolean} [isMinimized] add a filter on if the automaton is minimize
 * 
 * @returns {Array} array of titles of filtered automaton 
 */
async function search(token,isCompleted=null,isReachable=null,isCoreachable=null,isDeterminized=null,isMinimized=null){
    const response = await fetch(url + '/search', {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "iscompleted":isCompleted,
            "isreachable":isReachable,
            "iscoreachable":isCoreachable,
            "isdeterminized":isDeterminized,
            "isminimized":isMinimized,
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.titles;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Get the 'title' stored automaton
 *
 * @method
 * @async
 * @param {String} title title of the automaton loaded
 * @param {String} token the authorization token
 * 
 * @returns {Automaton}
 */
async function load(title, token) {
    const response = await fetch(url + "/load", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: title
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return json2automaton(data.automaton);
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Remove the 'title' automaton from the database
 *
 * @method
 * @async
 * @param {String} title title of the automaton to remove
 * @param {String} token the authorization token
 * 
 * @returns {Boolean} true if the 'title' automaton was well deleted, false otherwise 
 */
async function remove(title, token) {
    const response = await fetch(url + "/remove", {
        method: "POST",
        headers: {
            "authorization": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: title
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.deleted;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}


/**
 * Method to register to the api throught the client.
 * @method
 * @async
 * @param {String} username 
 * @param {String} password 
 */
async function register(username, password){
    const response = await fetch(url + "/register", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.token;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

/**
 * Method to log in to get your token api using your credentials
 * @method
 * @async
 * @param {String} username 
 * @param {String} password 
 */
async function getToken(username, password){
    const response = await fetch(url + "/gettoken", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });
    const data = await response.json();
    if (verify_response(response)) {
        return data.token;
    } else {
        throw new Error("Error when calling the API : " + data.error);
    }
}

exports.automaton2regex = automaton2regex;
exports.automaton2unixregex = automaton2unixregex;
exports.BFS = BFS;
exports.complement = complement;
exports.complete = complete;
exports.isCompleted = isCompleted;
exports.concatenate = concatenate;
exports.coreachableStates = coreachableStates;
exports.isCoreachable = isCoreachable;
exports.DFS = DFS;
exports.determinize = determinize;
exports.isDeterminized = isDeterminized;
exports.difference = difference;
exports.emptyLanguage = emptyLanguage;
exports.epsElimination = epsElimination;
exports.equivalence = equivalence;
exports.include = include;
exports.infiniteLanguage = infiniteLanguage;
exports.minimize = minimize;
exports.isMinimized = isMinimized;
exports.mirror = mirror; 
exports.normalize = normalize; 
exports.product = product; 
exports.reachableStates = reachableStates; 
exports.isReachable = isReachable; 
exports.regex2automaton = regex2automaton; 
exports.unionAutomaton = unionAutomaton;
exports.unixregex2automaton = unixregex2automaton; 
exports.store = store; 
exports.search = search; 
exports.load = load; 
exports.remove = remove;
exports.register = register;
exports.getToken = getToken;